package it.unisef.base;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServlet extends HttpServlet {

		protected void service(
				HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			
			response.setContentType("text/html");
			
			PrintWriter printWriter = response.getWriter();
			
			printWriter.println("<html><title>Hello Http Servlet</title><body>");
			printWriter.println("<h2>Hello World!</h2>");
			printWriter.println("</body></html>");
			
			printWriter.close();
		}
}
