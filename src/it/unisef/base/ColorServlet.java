package it.unisef.base;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ColorServlet extends HttpServlet {

	protected void doPost(
			HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		
		String color = request.getParameter("color");
		response.setContentType("text/html");
		
		PrintWriter printWriter = response.getWriter();
			
		
		printWriter.println("<html><title>Color Http Servlet</title><body bgcolor=" + 
								color + ">");
		
		printWriter.println("Il colore scelto �:");
		printWriter.println("<b><font size=\"20\" color=\"" + color + "\">" + color + "</b></font>");
		
		printWriter.println("<br><b> <a href=\"http://localhost:8080/ProvaServlet/color-switch\">" +
				"Torna Indietro" + "</a></b>");
		printWriter.println("</body></html>");
		
		printWriter.close();
				
	}
	
	protected void doGet(
			HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		response.setContentType("text/html");
		
		PrintWriter printWriter = response.getWriter();
			
		printWriter.println("<html>");
		printWriter.println("<body>");
		
		printWriter.println("<font size=\"20\"><form name=\"fm\"method=\"post\" action=\"http://localhost:8080/ProvaServlet/color-switch\">");
		printWriter.println("<b>Color:</b></br>");
		printWriter.println("<select name=\"color\">");
		printWriter.println("<option value=\"Red\">Red</option>");
		printWriter.println("<option value=\"Green\">Green</option>");
		printWriter.println("<option value=\"Blue\">Blue</option>");
		printWriter.println("</select>");
		printWriter.println("</font>");
			
		printWriter.println("<br /><br />");
			
		printWriter.println("<input type=\"submit\" value=\"submit\" />");

		printWriter.println("</form>");
		printWriter.println("</body>");
		printWriter.println("</html>");
		
		printWriter.close();
	}
	
	@Override	
	public void init() throws ServletException {
		 super.init();
		 System.out.println("Servlet di prova inizializzata.");
	};
	
	public void init(ServletConfig config) throws ServletException
    {
		super.init(config);
		System.out.println("Servlet di prova inizializzata.");
    }
			 
	public void destroy() {
		super.destroy();
		System.out.println("Servlet di prova rimossa.");		 
	}
}
